package com.example.diceroller

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rollBtn: Button = findViewById(R.id.rollButton)

        rollBtn.setOnClickListener { rollDice(true) }

        rollDice()
    }

    private fun rollDice(showToast: Boolean = false) {
        if (showToast) {
            Toast.makeText(this, "They see me rollin'", Toast.LENGTH_SHORT).show()
        }

        val dice = Dice(6)

        val rolledValueImage: ImageView = findViewById(R.id.rolledValueImage)

        val rolledValueImageResource = when (dice.roll()) {
            1 -> R.drawable.dice_1
            2 -> R.drawable.dice_2
            3 -> R.drawable.dice_3
            4 -> R.drawable.dice_4
            5 -> R.drawable.dice_5
            else -> R.drawable.dice_6
        }

        rolledValueImage.setImageResource(rolledValueImageResource)
        rolledValueImage.contentDescription = rolledValueImageResource.toString()
    }
}

class Dice(private val sides: Int) {
    fun roll(): Int {
        return (1..sides).random()
    }
}
